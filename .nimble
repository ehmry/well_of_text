let
  syndicate = builtins.getFlake "syndicate";
  pkgs =
    import <nixpkgs> { overlays = (builtins.attrValues syndicate.overlays); };
in pkgs.nim2Packages.buildNimPackage (finalAttrs: prevAttrs: {
  pname = "immutulator";
  version = "unstable";
  nativeBuildInputs = [ pkgs.pkg-config ];
  propagatedBuildInputs = [ pkgs.nim2Packages.getdns pkgs.nim2Packages.sdl2 ];
})
