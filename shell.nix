let
  syndicate = builtins.getFlake "syndicate";
  pkgs =
    import <nixpkgs> { overlays = (builtins.attrValues syndicate.overlays); };
in pkgs.nim2Packages.buildNimPackage (finalAttrs: prevAttrs: {
  pname = "well_of_text";
  version = "unstable";
  nativeBuildInputs = [ pkgs.pkg-config ];
  propagatedBuildInputs = [ pkgs.nim2Packages.getdns pkgs.nim2Packages.sdl2 ];
})
