# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, options, os, streams, tables]
import preserves, syndicate, syndicate/[capabilities]
import bumpy, pixie
import sdl2
import ./wells

const typefacePath =
  "/nix/store/ay5vhxszmibk0nrhx1vid4nhgvgdniq8-corefonts-1/share/fonts/truetype/Trebuchet_MS.ttf"

proc unixSocketPath: Unix =
  result.path = getEnv("SYNDICATE_SOCK")
  if result.path == "":
    result.path = getEnv("XDG_RUNTIME_DIR", "/run/user/1000") / "dataspace"

proc envStep: Assertion =
  var s = getEnv("SYNDICATE_STEP")
  if s != "": parsePreserves(s, Cap)
  else: capabilities.mint().toPreserve(Cap)

type
  SdlError = object of CatchableError

template check(res: cint) =
  if res != 0:
    let msg = $sdl2.getError()
    raise newException(SdlError, msg)

template check(res: SDL_Return) =
  if res == SdlError:
    let msg = $sdl2.getError()
    raise newException(SdlError, msg)

func toVec2(p: Point): Vec2 = vec2(float p.x, float p.y)

const
  amask = uint32 0xff000000
  rmask = uint32 0x000000ff
  gmask = uint32 0x0000ff00
  bmask = uint32 0x00ff0000

type
  App = ref object
    screen: Image
    window: WindowPtr
    renderer: RendererPtr
    rect: bumpy.Rect
    viewPoint: Vec2
    well: Well
    zoomFactor: float

func rect(img: Image): bumpy.Rect =
  result.w = float img.width
  result.h = float img.height

proc newApp(): App =
  ## Create a new square plane of `length` pixels.
  result = App(zoomFactor: 1.0)
  result.window = createWindow(
      "well of text",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      600, 800,
      SDL_WINDOW_RESIZABLE,
    )
  result.renderer = createRenderer(result.window, -1, 0)
  var
    info: RendererInfo
    # mode: DisplayMode
  check getRendererInfo(result.renderer, addr info)
  echo "SDL Renderer: ", info.name
  echo "SDL maximum texture size: ", info.max_texture_width, "x", info.max_texture_height
  #check getDisplayMode(result.window, mode)
  let (w, h) = result.window.getSize
  result.well = newWell(w, h)

func toSdl(rect: bumpy.Rect): sdl2.Rect =
  (result.x, result.y, result.w, result.h) =
    (cint rect.x, cint rect.y, cint rect.w, cint rect.h)

proc redraw(app: App) =
  assert app.zoomFactor != 0.0
  app.renderer.setDrawColor(0xf8, 0xf8, 0xf8)
  app.renderer.clear()
  var
    (w, h) = app.window.getSize
    wh = vec2(float w, float h)
    viewPort = rect(app.viewPoint, wh / app.zoomFactor)
  for intersect in app.well.intersectingPanes(viewPort):
    let texture = app.well.texture(intersect.index, app.renderer)
    if texture.isNil:
       break
    var
      sdlSrc = intersect.src.toSdl
      dst = rect(intersect.dst.xy - viewPort.xy, intersect.dst.wh)
    if app.zoomFactor != 1.0:
      dst = dst * app.zoomFactor
    var sdlDst = dst.toSdl
    app.renderer.copy(texture, addr sdlSrc, addr sdlDst)

  app.renderer.present()

proc resize(app: App) =
  ## Resize to new dimensions of the SDL window.
  redraw(app)

proc zoom(app: App; change: float) =
  app.zoomFactor = app.zoomFactor * (1.0 + ((1 / 8) * change))
  app.redraw()

proc pan(app: App; xy: Vec2) =
  app.viewPoint.xy = app.viewPoint.xy + (xy / app.zoomFactor)
  app.redraw()

proc recenter(app: App) =
  let (w, h) = app.window.getSize
  app.viewPoint = vec2(0, 0)
  app.zoomFactor = 1.0
  app.redraw()

proc main() =

  discard sdl2.init(INIT_TIMER or INIT_VIDEO or INIT_EVENTS)
  let app = newApp()
  app.redraw()

  let
    typeface = readTypeface(typefacePath)
    font = newFont(typeface)
      # TODO


  #[
  let
    unix = unixSocketPath()
    step = envStep()
  let actor = bootDataspace("chat") do (root: Cap; turn: var Turn):
    connect(turn, unix, step) do (turn: var Turn; ds: Cap):
      echo "connected to syndicate over UNIX-socket"

  asyncCheck actor.future
  ]#

  const
    sdlTimeout = 500
    asyncPollTimeout = 500

  let stream = newFileStream(stdin)
    # TODO
  var line: string
  while readLine(stream, line):
    # TODO
    app.well.append(line, font)
  app.redraw()

  var
    evt = sdl2.defaultEvent
    mousePanning: bool
  while true:
    # asyncdispatch.poll(0)
    if waitEventTimeout(evt, sdlTimeout):
      case evt.kind
      of MouseWheel:
        app.zoom(evt.wheel.y.float)
      of WindowEvent:
        if evt.window.event == WindowEvent_Resized:
          app.resize()
      of MouseMotion:
        if mousePanning:
          var xy = vec2(evt.motion.xrel.float, evt.motion.yrel.float)
          app.pan(xy * 2.0)
      of MouseButtonDown:
         case evt.button.button
         of BUTTON_MIDDLE:
           mousePanning = true
         else: discard
      of MouseButtonUp:
         case evt.button.button
         of BUTTON_MIDDLE:
           mousePanning = false
         else: discard
      of KeyUp:
        try:
          let code = evt.key.keysym.scancode.Scancode
          echo "code: ", code
          if code in {SDL_SCANCODE_SPACE, SDL_SCANCODE_ESCAPE}:
            app.recenter()
        except:
           # invalid event.key.keysym.sym sometimes arrive
          discard
      of KeyDown: discard
      of QuitEvent: quit(0)
      else:
        echo evt.kind

main()
